# Generated by Django 2.0.7 on 2018-08-01 04:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_auto_20180801_0143'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blog',
            name='sumario',
            field=models.CharField(default='', max_length=20),
        ),
    ]
