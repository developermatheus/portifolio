from django.db import models

# Create your models here.

# Ou eu faço um campo de sumário, ou faço a função dele pra pegar do body truncando.
class Blog(models.Model):
    title = models.CharField(max_length=200)
    sumario = models.CharField(max_length=20, blank=False, null=True)
    pub_date = models.DateTimeField()
    body = models.TextField(max_length=5000)
    image = models.ImageField(upload_to='images/')

    def __str__(self):
        return self.title

    def summary(self):
        return self.body[:10]

    def data_formatada(self):
        return self.pub_date.strftime('%d / %m / %y')